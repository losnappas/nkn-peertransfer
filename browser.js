let $ = require('jquery')
let Clipboard = require('clipboard')
let version = 'v' + require('./package.json').version
const comlink = require('comlink');

const worker = comlink.wrap(new Worker('pt-nkn.worker.js'));
let file, addr, key, interval
let peers = []

$('#version').html(version)
attachListeners()
initialize()
if (location.protocol === 'https:') {
  $('#https-notice').show()
}


;(async () => {
  const { file, name } = await (await worker.file)
  reset()
  let objectUrl = URL.createObjectURL(file)
  $('#step3 a').attr('href', objectUrl).attr('download', '' + name)
  setTimeout(x => document.getElementById('downloadLink').click(), 400)
  step(3)
})();
function attachListeners () {
  new Clipboard('.btn') // eslint-disable-line

  window.onbeforeunload = x => reset()

  let button = $('#step1 .button')
  button.click(x => $('#send-input').click())

  $(document).on('change', '#send-input', async e => {
    // the user selected a file and wants to send it
    $('.url').val(`${window.location.origin + window.location.pathname}#${key}`)
    $('body').attr('class', 'send')
    file = e.target.files[0]
    $('.if-send .size').text(file.size)
    await worker.upload(file)
    step(2)
  })

  $('a.back').click(function () {
    window.location.hash = ''
    button.off('click')
    $('#send-input').replaceWith(function () { return $(this).clone() })
    button.click(x => $('#send-input').click())
    reset()
    initialize()
  })
}

async function initialize () {
  step(1)
  addr = window.location.hash.substr(1)
  key = addr || await worker.addr
  if (!addr) bootAnimation()
  handlePeers()

  interval = setInterval(async () => {
    const transferred = await worker.transferred() || 0
    const s = parseInt($('.if-send .size').text() || 1) || 1;
    $('.if-send .transferred').text(`${Math.floor(transferred / s * 100)}%`)
  }, 1000)
}

function handlePeers () {
    if (!addr) {
      return
    }
    clearInterval(interval)
    interval = setInterval(async () => {
      const transferred = await worker.transferred() || 0
      const s = parseInt($('.if-receive .size').text() || 1) || 1
      $('.if-receive .transferred').text(`${Math.floor(transferred / s * 100)}%`)
    }, 1000)
    step(2)
    const fileMeta = worker.request(addr)
    fileMeta.then(async (fileMeta) => {
      fileMeta = await fileMeta
      $('.if-receive .size').text(fileMeta.size)
    })
    $('body').attr('class', 'receive')
    step(2)
    var downloadBar = $('#step2 .button')
    downloadBar.css('background-repeat', 'no-repeat')
    downloadBar.css('background-position', '-240px 0')
    downloadBar.css('background-image', 'url(assets/green.png)')
}

function reset () {
  clearInterval(interval)
  peers = []
}

function bootAnimation () {
  let button = $('#step1 .button')
  button.css('cursor', 'default')
  setTimeout(function () {
    button.attr('class', 'button green send')
    button.html('send a file')
    button.toggleClass('send')
    button.toggleClass('browse')
    button.css('cursor', 'pointer')
  }, 200)
}

function step (i) {
  let stage = $('#stage')
  let back = $('a.back')
  if (i === 1) back.fadeOut()
  else $('a.back').fadeIn()
  stage.css('top', (-(i - 1) * 100) + '%')
}

window.onhashchange = () => location.reload()
