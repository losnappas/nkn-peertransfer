'use strict'
self.window = self
require('web-streams-polyfill/es6')
const webStreams = require('web-streams-node')
const FileReadStream = require('filestream/read')
const FileWriteStream = require('filestream/write')
const comlink = require('comlink')
const nkn = require('nkn-sdk')

const numSubClients = 4
const sessionConfig = { mtu: 16000 }
var client
var file
var type, name, size, transferred = 0, interval
const deferred = (() => {
  let r, re
  const y = new Promise((a, b) => {
    r = a
    re = b
  })
  return { promise: y, resolve: r, reject: re }
})()

client = new nkn.MultiClient({ numSubClients, sessionConfig, worker: true })

const clientready = new Promise(resolve => client.onConnect(resolve));

client.listen()

client.onSession(async (session) => {
  console.log(session.localAddr, 'accepted a session from', session.remoteAddr)

  // Keep track of dl
  clearInterval(interval)
  interval = setInterval(() => {
    transferred = session.bytesRead
  }, 1000)

  const uploadStream = webStreams.toNodeReadable(session.getReadableStream())
  const writer = new FileWriteStream()
  uploadStream.pipe(writer).on('file', function (file) {
    deferred.resolve({file, name})
  })
})

client.onMessage(async ({ src }) => {
  console.log('Request from:', src)
  const session = await client.dial(src)

  clearInterval(interval)
  interval = setInterval(() => {
    transferred = session.bytesWrite
  }, 1000)

  session.setLinger(-1)
  console.log(session.localAddr, 'dialed a session to', session.remoteAddr)
  await clientready

  const uploadStream = (new FileReadStream(file))
  const uploadStreamWeb = webStreams.toWebReadableStream(uploadStream)
  const sessionStream = session.getWritableStream(true)

  uploadStreamWeb.pipeTo(sessionStream)
    .then(() => console.log('Sent to:', src)).catch(e => console.error('on send error: ', e))

  return JSON.stringify({ type: file.type, name: file.name, size: file.size })
})

comlink.expose({
  async request (addr) {
    // await new Promise(r => setTimeout(r, 500))
    console.log('sent to', addr)
    await clientready
    return client.send(addr, '').then(payload => {
      const x = JSON.parse(payload)
      type = x.type
      name = x.name
      size = x.size
      return x;
    })
  },
  transferred() { return transferred },
  upload (infile) {
    file = infile
  },
  addr: client.addr,
  file: deferred.promise
})
